import React from 'react';
import './App.css';
import AutoSearch from './components/AutoSearch';

function App() {
	return (
		<div className="App">
			<section className="App-main">
				<AutoSearch />
			</section>
		</div>
	);
}

export default App;